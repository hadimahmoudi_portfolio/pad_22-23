import mysql.connector as mysql
import secrets
import hashlib


#A function that takes a username and a password, hashes the password and inserts it in to the
# database. The function returns a status. Status = 0 means user added and status = 1 means user 
# already exists (username taken).
def sign_user_up(username, password):
    try:
        #Establish DB connection with signup user
        db = mysql.connect(
        host = "database",
        user = "signup",
        passwd = "@#1sAca2hvVqa",
        database = "paddb")
            
        #create a hex salt with 16 bits and encrypt the password with the salt.
        salt = secrets.token_hex(16)
        password = password + salt
        password = password.encode('UTF-8')
        password = hashlib.sha256(password)
        password = password.hexdigest()

        #Insert user data with a prepared statement
        insert_query = "INSERT INTO `user` (`username`, `salt`, `password`) VALUES (%s,%s,%s);"
        sql_input = [username, salt, password]
        mycursor = db.cursor()
        mycursor.execute(insert_query, sql_input)

        #insert ctf data for user (all flags = 0 aka uncaptured)
        #IF NEW CTF IS ADDED: ADD NEW INSERT QUERY OTHERWISE NEW USERS CANNOT ENTER FLAGS FOR NEW CTF!!!
        sql_input = [username]
        insert_query = "INSERT INTO `user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES (%s, 1, 0, NULL, NULL);"
        mycursor.execute(insert_query, sql_input)
        insert_query = "INSERT INTO `user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES (%s, 2, 0, NULL, NULL);"
        mycursor.execute(insert_query, sql_input)
        insert_query = "INSERT INTO `user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES (%s, 3, 0, NULL, NULL);"
        mycursor.execute(insert_query, sql_input)
        insert_query = "INSERT INTO `user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES (%s, 4, 0, 0, NULL);"
        mycursor.execute(insert_query, sql_input)
        #These are template querys and can be used to copy and pase
        # insert_query = "INSERT INTO `user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES (%s, int, NULL, NULL, NULL);"
        # mycursor.execute(insert_query, sql_input)
        db.commit()

        mycursor.close()
        db.close()

    #if user already exists display error
    except mysql.IntegrityError:
        mycursor.close()
        db.close()
        status = 1
        return status

    #if user is succesfully added redirect to login
    else:
        status = 0
        return status


#A function that checks users creds. It takes a username and a password, and will return a status.
# status = 0 means password and username match, status = 1 means username and/or password do not 
# match and status = 2 means invalid username.
def user_login(input_username, input_passwd):
    try:
        #Establish DB connection with login user.
        db = mysql.connect(
        host = "database",
        user = "login",
        passwd = "*2Sasf@csAas3",
        database = "paddb")

        #A prepared statement to get username, salt and hashed password from db.
        mycursor = db.cursor()
        sql = "SELECT username, salt, password FROM user WHERE username = %s"
        sql_param = input_username
        mycursor.execute(sql, sql_param)
        result = mycursor.fetchone()

        #Assign correct vars to sql result.
        username = result[0]
        salt = result[1]
        password = result[2]

        #Revert username to correct format.
        input_username = input_username[0]
        
        #Hash the password entered by user.
        input_passwd = input_passwd + salt
        input_passwd = hashlib.sha256(input_passwd.encode('utf-8')).hexdigest()

        mycursor.close()
        db.close()

        #check if username and passwords match
        if input_username == username and input_passwd == password:
            status = 0
            return status

        #if username and password dont match display error message 
        else:
            status = 1
            return status

    #if username doesn't exist an error message will be displayed
    except TypeError:

            status = 2
            return status 


# This is a function that allows users to delete their account. It takes a username and 
# returns nothing special.
def delete_user(username):
     #Establish DB connection with del user.
    db = mysql.connect(
            host = "database",
            user = "del",
            passwd = "w7vL*5*@c_C!3",
            database = "paddb")
           
    # prepared statement to delete user from db.
    mycursor = db.cursor()
    sql = f"DELETE FROM user WHERE username = %s"
    sql_param = [username]
    mycursor.execute(sql, sql_param)
    db.commit()
    mycursor.close()
    db.close()
    
    return 