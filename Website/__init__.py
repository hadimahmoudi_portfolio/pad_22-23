#These modules will need to be imported, the modules starting with a dot are local modules.
import time
from flask import Flask, url_for, request, redirect, render_template, session, flash
import mysql.connector as mysql
import secrets
from .ctf_module import *
from .users_module import *
from .audit_module import *

app = Flask(__name__)

#Keeps client-side sessions secure
app.secret_key = secrets.token_bytes(16)

#If users enter 'https://localhost:443' they wil be rerouted to the right url, based on a session cookie.
@app.route("/")
def welcome():
    if 'username' in session:
        return redirect(url_for("challenges"))
    else:
        return redirect(url_for("login"))

#This route handles the signup proces. For this it uses functions from the user_module.
# When a post request is sent (by clicking the button) this function will take the entered data,
# and handle it accordingly. 
@app.route("/aanmelden", methods=["POST", "GET"])
def aanmelden():
    if 'username' in session:
        return redirect(url_for('welcome'))
    else:
        if request.method == "POST":

            username = request.form["gebruikersnaam"]
            password = request.form["wachtwoord"]
            password_repeat = request.form["wachtwoord-herhaling"]

            #if passwords match continue with data insertion.
            if password == password_repeat:
                status = sign_user_up(username, password)

                #User was succesfully added. 
                if status == 0:
                    flash("Gebruiker gemaakt, log nu in")
                    audit(username, 0)
                    return redirect(url_for("login"))
                #Username was taken.    
                elif status == 1:
                    flash("gebruikersnaam niet beschikbaar")
                    audit(username, 1)
                    return render_template('signup.html')

            #if passwords don't match let user try again.
            else:
                flash("De wachtwoorden komen niet overeen, probeer het opnieuw")
                audit(username, 1)
                return render_template('signup.html')
                
        else:
            return render_template("signup.html")

#This route handles the login process. For this it uses functions from the user_module. 
# When a post request is sent (by clicking the button) this function will take the entered data,
# and handle it accordingly. Just like the sign-up route
@app.route("/login", methods=["POST", "GET"])
def login():
    #check if user is signed in
    if 'username' in session:
        return redirect(url_for('welcome'))

    #if user is not signed in let them log in    
    else:
        if request.method == "POST":
            input_username = request.form["username"]
            input_username = [input_username]
            input_passwd = request.form["passwd"]

            status = user_login(input_username, input_passwd)

            # User logged in
            if status == 0:
                session['username'] = input_username[0]
                audit(input_username[0], 2)
                return redirect(url_for("welcome"))

            # Unsuccesfull log in attempts  
            elif status == 1:
                flash("gebruikersnaam en/of wachtwoord incorrect")
                audit(input_username[0], 3)
                return render_template("login.html")
            elif status == 2:
                flash("gebruikersnaam en/of wachtwoord incorrect")
                audit(input_username[0], 3)
                return render_template("login.html")

        #display page if not a post request
        else:
            return render_template("login.html")

#This route lets users manage their profile: they can delete their account according to the "AVG" law.
@app.route("/profile", methods=["POST", "GET"])
def profile():
    if 'username' in session:
        username = session["username"]
        if request.method == "POST":
            delete_user(username)
            stop_all_ctf(username)          #Checks and stops containers if they are still running.
            audit(username, 7)
            audit(username, 8)
            session.pop('username', None)
            return render_template("profile.html", status = "Account verwijderd!", name = username)

        else:
            return render_template("profile.html", name = username)
    
    else:
        return redirect(url_for('login'))

#This route lets user sign out. It does not do anything special, but just pops the username from the session.    
@app.route("/uitloggen")
def uitloggen():
    #check if user is logged in
    if 'username' in session:
        #delete user from session aka log them out
        session.pop('username', None)
        return redirect(url_for('login'))

    #if user not signed in tell them and make them login first
    else:
        flash("Om deze pagina te bezoeken moet u ingelogd zijn")
        return redirect(url_for("login"))

#This route shows all available CTFs and displays if users have captured flags for them.
# It does this with a special database user.
@app.route("/ctf")
def challenges():
    if 'username' in session:
        username = session["username"]

        #Establish DB connection with flag user
        db = mysql.connect(
        host = "database",
        # host = "localhost",
        user = "flag",
        passwd = "8iAnDu#@a4%ac",
        database = "paddb")

        #get all flags and statuses for logged in user
        #fetchall can be used because query is sql injection protected and only insensisitive data is retrieved
        mycursor = db.cursor()
        sql = f"SELECT flag1_status, flag2_status, flag3_status from user_flags WHERE user_username = %s;"
        sql_param = [username]
        mycursor.execute(sql, sql_param)
        result = mycursor.fetchall()

        tally = 0       #The tally is used to enter data in the list.
        score = []

        #Loop through tuple result and the items within the tuple
        for flags in result:
            for status in flags:

                if status == 1:     #flag is captured
                    tally = tally + 1
            score.append(tally)
            tally = 0 

        return render_template("challenges.html", count=score)

    else:
        flash("Om deze pagina te bezoeken moet u ingelogd zijn")
        return redirect(url_for("login"))

#This route handles the starting of challenges. It takes the INT from the url to start a CTF
@app.route("/ctf<int:id>", methods=["POST", "GET"])
#take id from url and use it to start matching container
def startup(id):
    if 'username' in session:
        username = session["username"]  
        port_2, port_1 = random_port_gen()
        status = start_ctf(id, username, port_1, port_2)

        if status == 0:     #Challenge started without problem
            audit(username, 4)
            return redirect(f"http://localhost:{port_1}")

        elif status == 1:
            
            audit(username, 5)
            while status == 1:      #Port already in use
                port_2, port_1 = random_port_gen()
                status = start_ctf(id, username, port_1, port_2)
                time.sleep(5)

            audit(username, 4)
            return redirect(f"http://localhost:{port_1}")

        elif status == 2:       #Name already in use, challenge already up
            port = get_port(id, username)
            return redirect(f"http://localhost:{port}")

        else:
            #Should not be shown ever.
            return "Something went wrong!"
    else:
        flash("Om deze pagina te bezoeken moet u ingelogd zijn")
        return redirect(url_for("login"))

#This route handles the stopping of all CTFs for the logged in user.
@app.route("/stop", methods=["POST", "GET"])
#stops all active containers for a certain user
def stop():
    if 'username' in session:
        username = session["username"]
        stop_all_ctf(username)
        audit(username, 7)

        return redirect(url_for("challenges"))
    else:
        flash("Om deze pagina te bezoeken moet u ingelogd zijn")
        return redirect(url_for("login"))  

#This route show hints for every page, it also uses the INT from the url to specify the CTF
@app.route("/hints<int:id>")
def hints(id):
    if 'username' in session:
        return render_template(f"hints_ctf{id}.html")
    else:
        flash("Om deze pagina te bezoeken moet u ingelogd zijn")
        return redirect(url_for("login"))

#This route handles the flag checking. 
@app.route("/flags", methods=["POST", "GET"])
def flags():
    #check if user is signed in, otherwise make them sign in first
    if 'username'  in session:
        if request.method == "POST":
            input_flag = request.form["flag1"]
            username = session["username"]
            
            #Establish DB connection with flag user
            db = mysql.connect(
            host = "database",
            # host = "localhost",
            user = "flag",
            passwd = "8iAnDu#@a4%ac",
            database = "paddb")

            #get all flags from db
            mycursor = db.cursor()
            sql = "select ctf_id, flag1, flag2, flag3  from ctf;"
            mycursor.execute(sql)
            flags = mycursor.fetchall()

            #check if entered flag exists and get matching ctf id and status for user
            for flag in flags:
                if input_flag in flag:
                    ctf_id = flag[0]
                    
                    for i in range(len(flag)):
                        if input_flag == flag[i]:
                            flag_pos = f"flag{i}_status"
                        
                    #check if entered flag is already submitted
                    sql = f"SELECT {flag_pos} from user_flags WHERE user_username = %s and ctf_id = %s;"
                    sql_param = [username, ctf_id]
                    mycursor.execute(sql, sql_param)
                    flag_status = mycursor.fetchone()
                    
                    #if flag status = 0 upadate status to 1 (captured)
                    if flag_status[0] == 0:
                        sql = f"UPDATE user_flags SET {flag_pos} = '1' WHERE user_username = %s and ctf_id = %s;"
                        sql_param = [username, ctf_id]
                        mycursor.execute(sql, sql_param)

                        db.commit()
                        mycursor.close()
                        db.close()
                        audit(username, 6)
                        return render_template("flags.html", status = "Gefeliciteerd, je hebt een flag gevonden!")

                    #if flag is already captured display "error" message   
                    else:
                        return render_template("flags.html", status = "flag al ingevuld...")
                
            #if flag doesn't exist display error message
            return render_template("flags.html", status = "flag bestaat niet")

        #if user is not sending post request display site
        else:
            return render_template("flags.html")

    #send user to login page if not logged in
    else:
        return redirect(url_for("login"))

#This route shows our terms and conditions.
@app.route("/terms-and-conditions")
def terms():
    return render_template("t&c.html")
     
if __name__ == "__main__":
    app.run()
