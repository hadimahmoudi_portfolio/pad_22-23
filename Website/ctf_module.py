from random import *
import docker

client = docker.from_env()

#A simple function that will return psuedo random ports.
def random_port_gen():
    flag = False
    while flag == False:
        port_1 = randint(49152, 65535)
        port_2 = randint(49152, 65535)
        
        if port_2 != port_1:
            flag = True
        else:
            continue
    return port_2, port_1


#Function that will start specified challenge based on challenge id, username and given ports. It returns a status code
# based on the procesflow. If no errors are encounterd the challenge has succesfully started and status = 0 
# will be returned. The function could encounter two different errors: ports that are already in use and the challenge 
# is already up and running. Respectively this will return status = 1 and status = 2 . If status = (error_message) is returned
# something broke down and error logs should be reviewd.  
def start_ctf(id, username, port_1, port_2):

    #Docker will try to start a challenge based on id.
    try:
        if id == 1:
            con = client.containers.run(image=f"{id}-fe", name=f"fe-challenge{id}-{username}", detach=True, ports={80:port_1})
            status = 0
            

        elif id == 2 or id==3:
            network = client.networks.create(name=f"network-challenge{id}-{username}", check_duplicate=True)
            con = client.containers.run(image=f"{id}-fe", name=f"fe-challenge{id}-{username}", detach=True, ports={80:port_1})
            con = client.containers.run(image=f"{id}-db", name=f"db-challenge{id}-{username}", detach=True, ports={3306:port_2})

            network.connect(container=f"db-challenge{id}-{username}", aliases=["database"])
            network.connect(container=f"fe-challenge{id}-{username}")
            status = 0

        elif id == 4:
            con = client.containers.run(image=f"{id}-fe", name=f"fe-challenge{id}-{username}", detach=True, ports={80:port_1, 8356:port_2})
            status = 0

    except docker.errors.APIError as error:

        #Docker could not allocate on of the ports, this is because one of the generated ports is already in use
        # by an other player or beacause two or more of the generated ports are the same. 
        if error.response.status_code == 500:

            #Docker will try to remove the front-end container the user tried to start.
            try:
                con = client.containers.get(f"fe-challenge{id}-{username}")
                con.remove(v=True)
                
            except docker.errors.APIError as error:

                #Docker could not find the front-end container and therefore does not have to be removed.
                if error.response.status_code == 404:
                   pass

                #Docker could not remove the container because it was still running. This only 
                # happens when the front-end container could start up because it got a unused port.
                else:
                    con.kill() 
                    con.remove(v=True)

            #Python will check the id of the challenge because some challenges (1 and 4) do not have
            # a database. Letting it check for containers that are 100% not there would mean that 
            # we are wasting resources.
            if id == 1 or id == 4:                         
                status = 1
                return status

            else:
                pass

            #Python will try to remove the database container the use tried to start.
            try:
                con = client.containers.get(f"db-challenge{id}-{username}")
                con.remove(v=True)

            except docker.errors.APIError as error:
                #Docker could not find the database container and therefore does not have to be removed.
                #This only happens if the front-end container could not be started due to conflicting
                # ports.
                if error.response.status_code == 404:
                   pass

                else:
                    print(error)
                    
            #Because the docker network is made before the containers are started, there will
            # always be docker network unless one already exists for the user and id combo.
            # This network does not need to exist in this case, therefore it is removed.
            network = client.networks.get(f"network-challenge{id}-{username}")
            network.remove()

            status = 1
            return status

        elif error.response.status_code == 409:
            #Whenever docker encounters a container or network(because check_duplicate=True in network.create()) with the
            # same name it will give out error 409. This means that the challange is already running.
            status = 2
            return status
            
        else:
            #Something has gone wrong if this is returned, please read error log.
            status = error
            return status

    else:
        return status


#Simple function that returns port from already running container, id and username.
def get_port(id, username):
    #Ask docker for container info
    container = client.containers.get(f"fe-challenge{id}-{username}")
    #container.ports is a dictionary with list as value. In this list the first item
    # is another dictionary. 
    tcp = container.ports.get("80/tcp")
    port = tcp[0].get("HostPort")
    return port

#Function that stops all active containers for a given username, prints out the process and returns nothing.
def stop_all_ctf(username):
    for i in range(1,5):
        id = i
        
        #check for running database containers
        try:
            con = client.containers.get(f"db-challenge{id}-{username}")
            con.kill()
            con.remove(v=True)
            print("del: db", id)
            
        except docker.errors.NotFound as error:
            print("not found: db", id)

        except docker.errors.APIError as error:
            con.remove(v=True)

        #check for running front-end containers
        try:
            con = client.containers.get(f"fe-challenge{id}-{username}")
            con.kill()
            con.remove(v=True)
            print("del: fe", id)
            
        except docker.errors.NotFound as error:
            print("not found: fe", id)
        except docker.errors.APIError as error:
            con.remove(v=True)

            
        #check for running networks
        try:
            network = client.networks.get(f"network-challenge{id}-{username}")
            network.remove()
            print("del: net", id)
            
        except docker.errors.NotFound as error:
            print("not found: net", id)
    return None