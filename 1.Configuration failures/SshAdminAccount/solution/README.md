# SshAdminAccount 

## Solution

----------- Path to flag 1: -----------
1.	Lees het bericht en pak de ip uit dat bericht
2.	Scan ip voor open poorten
3.	Vind de ssh poort
4.	Brute force het wachtwoord met bijvoorbeeld Hydra
	Wachtwoord: 123456
5.	Het wachtwoord is de eerste flag
	Flag: flag{123456}

Hints:
“Hoe kan je een ip adres scannen om te zien welke services welke poorten gebruiken?”
“Hoe kan je een wachtwoord van een gebruiker raden? Misschien zegt “rockyou” iets?”


----------- Path to flag 2: -----------
1.	Kijk verder op het systeem
2.	Ga naar /home/important-files
3.	Er staan 3 text files met honderden lijnen text
4.	Gebruik bijvoorbeeld grep om te zoeken naar “flag” in de inhoud van de files (grep flag text, text2, text3)
	Flag: flag{g5hf-ko6h-kl7g}

Hints:
“Welke directories hebben interessante namen?”
“Hoe kan je een woord opzoeken in een groot text bestand?”
