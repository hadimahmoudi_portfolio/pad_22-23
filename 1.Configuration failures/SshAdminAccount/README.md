# <Configuration failure> - <SshAdminAccount> 

## Description

een omgeving wordt opgestart waarin een ssh server draait. met nmap kan je de ssh port vinden en met bijvoorbeeld hydra kan je het wachtwoord kraken. je krijgt een message als je begint met  de challenge.


## Deploy

wordt gedeployed via de frontend. of je kan hem zelf deployen met `docker compose up`

## Challenge

Iemand heeft je een bericht doorgestuurd: 

Hey Hydra,
you really need to change your user password on the machine running your website, it would be super easy to crack! And maybe change your username to something other than your name
-IT Department 

flag1: flag{123456}
flag2: flag{g5hf-ko6h-kl7g}