# SshAdminAcount

## Solution
----------- Path to flag: -----------
1.  Zoek het bericht van Julius Caesar met de volgende inhoud: 
    Lfzijt yj mnh jxxj, szsh ijrzr xjhwjyzr rjzr wjajqfwj utxxzr. ij kqfl nx G3xy-h1um3w

    HINT: "Senpai spreekt Japans, Luigi spreekt Italiaans en Vlad spreekt Russisch, maar spreekt de rest ook zijn eigen taal?"
    HINT: "Er zit een volgorde in de berichten, maar welke volgorde is dit?"

2.  Haal deze door een cipher decoder heen.  (over te slaan als gebruiker hint volledig gebruikt of gelijk inziet dat het een Caesar cipher is.)

3.  Voer het bericht in op een website die de Ceaser cipher kan ontsleutelen. Hieruit volgt een bericht waar de flag in vermeld staat: 
    B3st-c1ph3r

flag: flag{B3st-c1ph3r}


