# <Cryptographic failure> - <SecretMessage>

## Description

Op een forum kom je een raar bericht tegen, als je deze decrypt met een caeser cypher decoder krijg je de flag

## Deployment

Wordt gedeployed via de frontend. of je kan hem zelf deployen door de docker image te maken met `docker build -t {naam}` en vervolgens `docker run {naam} -p 80:{poort}`

## Challenge

Je hebt een website gevonden waar iedereen een bericht kan plaatsen. Sommige berichten zijn in het Nederlands geschreven en sommige zijn in een andere taal geschreven. Maar van een aantal van deze berichten kan je niet bepalen in welke taal ze geschreven zijn… Kan jij erachter komen wat er gezegd wordt? Deze CTF heeft als doel om de speler bij te brengen hoe een simpele CTF in elkaar steekt en hoe het platform werkt.

flag: flag{B3st-c1ph3r}