from flask import Flask, render_template

app = Flask(__name__)

#This route makes sure that the 6chan board is displayed.
@app.route("/")
def message_board():
    return render_template("board1.html")
 
if __name__ == "__main__":
    app.run()