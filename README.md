# Project Agile Development (PAD) <2022>

The client of this project is Cyber Security Learning Route HBO-ICT (Amsterdam University of Applied Sciences), represented by the learning route coordinator Tiemen Mink, advised by CS teachers from higher years.

Each team develops one or more modules for a Capture the Flag (CTF) for the client (Amsterdam University of Applied Sciences). Each team chooses one Cyber threat theme for which they create the modules.
A CTF is a competition in which a number of participants can demonstrate their hacking skills in a controlled environment.


# BSidesPDX CTF HvA Team A 
  
Capture the flag of Team A for PAD project based on the BSidesPDX framework. [bsides-ctf-framework](https://github.com/BSidesPDX/bsides-ctf-framework)

## Challenges

| Challenge Name                            | Category                              |
| ----------------------------------------- | ------------------------------------- |
| [SecretMessage]            				| cryptographic failures                |
| [Sshadminaccount]       					| configuration failures                |
| [Lethal-Injection]         				| injection                             |
| [GereedschapShop]       					| injection                             |

## Local Deployment

### Requirements: 
Docker & Build Tools

### Build:
Via the command line, the challenge must first be built in each challenge directory with docker:

1. `docker build -t name_of_container .`

Then you can start the front and back end

2. `docker-compose up`

### Down

5. `docker-compose kill` to stop the containers
4. `docker rm pad_website_1 pad_database_1` to remove the containers
5. `Docker image rm pad_website pad_database` to delete the images

## Documentation

For more insight about this product, refer to the documents folder.
