-- Create user for sign up

-- SELECT CURRENT_USER();

SELECT USER();

USE `paddb`;

-- Create user for sign-up
CREATE USER signup IDENTIFIED WITH caching_sha2_password BY '@#1sAca2hvVqa';
GRANT INSERT ON user TO 'signup';
GRANT INSERT ON user_flags TO 'signup';

-- Create user for login
CREATE USER login IDENTIFIED WITH caching_sha2_password BY '*2Sasf@csAas3';
GRANT SELECT ON user TO 'login';

-- Create user for flags
CREATE USER flag IDENTIFIED WITH caching_sha2_password BY '8iAnDu#@a4%ac';
GRANT SELECT ON ctf TO 'flag';
GRANT SELECT, UPDATE ON user_flags TO 'flag';

-- Create user for deleting account
CREATE USER del IDENTIFIED WITH caching_sha2_password BY 'w7vL*5*@c_C!3';
GRANT SELECT, DELETE ON user TO 'del';

COMMIT;