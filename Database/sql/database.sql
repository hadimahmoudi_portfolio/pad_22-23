-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema paddb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema paddb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `paddb` DEFAULT CHARACTER SET utf8 ;
USE `paddb` ;

-- -----------------------------------------------------
-- Table `paddb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `paddb`.`user` (
  `username` VARCHAR(45) NOT NULL,
  `salt` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`username`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `paddb`.`user_flags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `paddb`.`user_flags` (
  `user_username` VARCHAR(45) NOT NULL,
  `ctf_id` INT NOT NULL,
  `flag1_status` TINYINT NOT NULL,
  `flag2_status` TINYINT NULL,
  `flag3_status` TINYINT NULL,
  PRIMARY KEY (`user_username`, `ctf_id`),
  INDEX `fk_user_flags_user1_idx` (`user_username` ASC) VISIBLE,
  CONSTRAINT `fk_user_flags_user1`
    FOREIGN KEY (`user_username`)
    REFERENCES `paddb`.`user` (`username`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `paddb`.`ctf`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `paddb`.`ctf` (
  `name` VARCHAR(45) NOT NULL,
  `ctf_id` INT NOT NULL,
  `flag1` VARCHAR(45) NOT NULL,
  `flag2` VARCHAR(45) NULL,
  `flag3` VARCHAR(45) NULL,
  PRIMARY KEY (`name`, `ctf_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `paddb`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `paddb`;
INSERT INTO `paddb`.`user` (`username`, `salt`, `password`) VALUES ('bob', 'a', '44343534306a48e839974d4ecdfbb2ecfc0157c92665bdd6c1c48ab2ad5f8ad0');

COMMIT;


-- -----------------------------------------------------
-- Data for table `paddb`.`user_flags`
-- -----------------------------------------------------
START TRANSACTION;
USE `paddb`;
INSERT INTO `paddb`.`user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES ('bob', 1, 0, NULL, NULL);
INSERT INTO `paddb`.`user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES ('bob', 2, 0, NULL, NULL);
INSERT INTO `paddb`.`user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES ('bob', 3, 0, NULL, NULL);
INSERT INTO `paddb`.`user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES ('bob', 4, 0, 0, NULL);
-- INSERT INTO `paddb`.`user_flags` (`user_username`, `ctf_id`, `flag1_status`, `flag2_status`, `flag3_status`) VALUES ('bob', 3, 0, 0, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `paddb`.`ctf`
-- -----------------------------------------------------
START TRANSACTION;
USE `paddb`;
INSERT INTO `paddb`.`ctf` (`name`, `ctf_id`, `flag1`, `flag2`, `flag3`) VALUES ('6chan', 1, 'flag{B3st-c1ph3r}', NULL, NULL);
INSERT INTO `paddb`.`ctf` (`name`, `ctf_id`, `flag1`, `flag2`, `flag3`) VALUES ('Lethal Injection', 2, 'flag{1nJ3cT3D}', NULL, NULL);
INSERT INTO `paddb`.`ctf` (`name`, `ctf_id`, `flag1`, `flag2`, `flag3`) VALUES ('GereedschapShop', 3, 'flag{2zf3cZFd}', NULL, NULL);
INSERT INTO `paddb`.`ctf` (`name`, `ctf_id`, `flag1`, `flag2`, `flag3`) VALUES ('GereedschapShop', 4, 'flag{123456}', 'flag{g5hf-ko6h-kl7g}', NULL);
-- INSERT INTO `paddb`.`ctf` (`name`, `ctf_id`, `flag1`, `flag2`, `flag3`) VALUES ('SuperSecurePasswordManager', 3, 'test1', 'test2', NULL);
COMMIT;

