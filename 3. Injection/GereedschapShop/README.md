# <Injection> - <GereedschapShop>

## Description

Moeilijkere injectie challenge met meerdere injecties die uitgevoerd moeten worden.

## Deployment

wordt gedeployed via de frontend. of je kan hem zelf deployen met `docker compose up`

## Challenge

Je hebt online een gereedschap winkel gevonden om de gereedschap te kopen die je nodig hebt, maar het ziet er niet zo veilig uit. Er staan alleen een welkomst boodschap met  een zoekvak en verder geen andere informatie. Wat denk je te kunnen vinden via het zoekvak? Probeer maar!
Het doel van deze CTF is om de haakvaardigheden van de speler te testen en om te laten zien hoe je met de juiste SQL-query's andere informatie kan extraheren dan de website-eigenaar wilde.

flag: flag{2zf3cZFd}