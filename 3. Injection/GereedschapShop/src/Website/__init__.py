#Required libraries and models
from flask import Flask,  request, render_template
import mysql.connector as mysql


app = Flask(__name__)


#Keeps client-side sessions secure
app.secret_key = b'893401'

@app.route("/", methods=["POST", "GET"])
def welcome():

    if request.method == "POST":
        search = request.form['search']

        #Communicate with the built-in database in Docker container.
        db = mysql.connect(
                host = "database",
                user = "search",      
                passwd = "245*$5240",    
                database = "sql_hard_db")

        #The searched product must be more than 2 letters.
        if len(search) <= 2: 
            return render_template("index.html", text = "Te kort karakters! Zoek maar nog een keer met tenminste drie karakters.")

        #Prevent error messages if unauthorized characters are used.
        try:
            mycursor = db.cursor(buffered=True)
            sql = f"SELECT productname, price FROM product WHERE productname LIKE '%{search}%';"
            mycursor.execute(sql)
            result = mycursor.fetchall()
            mycursor.close()

            if result:
                headings = ("Naam", "Prijs (euro)")
                return render_template("index.html", headings = headings, result = result)

            else:
                return render_template("index.html", text = "Geen product gevonden!")

        except:
            return render_template("index.html", text = "Verkeerd karakter!")


    return render_template("index.html", text = "")

if __name__== "__main__":
    app.run()