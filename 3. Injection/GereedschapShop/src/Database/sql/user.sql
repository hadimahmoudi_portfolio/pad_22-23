SELECT USER ();

USE `sql_hard_db` ;

CREATE USER search IDENTIFIED WITH caching_sha2_password BY '245*$5240';
GRANT SELECT ON product TO 'search';
GRANT SELECT ON CtfFlag TO 'search';
COMMIT;