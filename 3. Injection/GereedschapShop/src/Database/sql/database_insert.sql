-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema sql_hard_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sql_hard_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sql_hard_db` DEFAULT CHARACTER SET utf8 ;
USE `sql_hard_db` ;

-- -----------------------------------------------------
-- Table `sql_hard_db`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sql_hard_db`.`product` (
  `productid` INT NOT NULL,
  `productname` VARCHAR(45) NOT NULL,
  `price` INT(3) NOT NULL,
  PRIMARY KEY (`productid`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `sql_hard_db`.`CtfFlag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sql_hard_db`.`CtfFlag` (
  `flagid` INT NOT NULL,
  `flag` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`flagid`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `sql_hard_db`.`product`
-- -----------------------------------------------------
START TRANSACTION;
USE `sql_hard_db`;
INSERT INTO `sql_hard_db`.`product` (`productid`, `productname`, `price`) VALUES (1, 'Hamer', 5);
INSERT INTO `sql_hard_db`.`product` (`productid`, `productname`, `price`) VALUES (6, 'HamerXL', 9);
INSERT INTO `sql_hard_db`.`product` (`productid`, `productname`, `price`) VALUES (2, 'Zaag', 7);
INSERT INTO `sql_hard_db`.`product` (`productid`, `productname`, `price`) VALUES (3, 'Schroevendraaier', 4);
INSERT INTO `sql_hard_db`.`product` (`productid`, `productname`, `price`) VALUES (4, 'Slijptol', 30);
INSERT INTO `sql_hard_db`.`product` (`productid`, `productname`, `price`) VALUES (5, 'Betonboor', 50);
COMMIT;

-- -----------------------------------------------------
-- Data for table `sql_hard_db`.`CtfFlag`
-- -----------------------------------------------------
START TRANSACTION;
USE `sql_hard_db`;
INSERT INTO `sql_hard_db`.`CtfFlag` (`flagid`, `flag`) VALUES (1, 'flag{2zf3cZFd}');
COMMIT;

