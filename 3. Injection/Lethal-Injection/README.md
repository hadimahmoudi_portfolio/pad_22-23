# <Injection> - <Lethal-Injection>

## Description

Injectie challenge met een 1 = 1 injection

## Deployment

wordt gedeployed via de frontend. of je kan hem zelf deployen met `docker compose up`

## Challenge

Je bent op een website gekomen die er niet veilig uitziet en bent benieuwd of er een soort beveiliging op zit. Er is alleen een login pagina te zien en verder geen andere informatie. In deze challenge kun je de uiterste basis van een SQL injectie oefenen en leren te begrijpen.

flag: flag{1nJ3cT3D}