-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema injectiondb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema injectiondb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `injectiondb` DEFAULT CHARACTER SET utf8 ;
USE `injectiondb` ;

-- -----------------------------------------------------
-- Table `injectiondb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `injectiondb`.`user` (
  `userid` INT NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`userid`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `injectiondb`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `injectiondb`;
INSERT INTO `injectiondb`.`user` (`userid`, `username`, `password`) VALUES (1, '9aad59c6bnell61a1ydk9i756efk7wccopues080otxl', '>4@D.{q$aTS;<peVMh#zkuUs;Mg}&*L#&C+6R&_@XhM_^=`B!SQ2Rr`d~9(wdm]$r{7Y8-268W3K)S\>q$ShDeGQbKYnrh\_`9by,*j{d;>x?#!T;u#$`Fd%/5,(+N8*U~ECgK^U?$MTH&k^Jqg.~yx&N].RQz(2by>E\P.rS>Az=%/`+R&_.A7bm=T>.f&8Ga&,>4&)yM8H6-Rs#mAT$H^FKM^Ek}t_/_YCyB]yW@vN`U3K%8{;admM/DKg@<');
COMMIT;

