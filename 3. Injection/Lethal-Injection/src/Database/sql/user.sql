USE `injectiondb` ;

CREATE USER login IDENTIFIED WITH caching_sha2_password BY '84Cb%#s9@01';
GRANT SELECT ON user TO 'login';
COMMIT;
