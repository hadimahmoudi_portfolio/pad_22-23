from flask import Flask, request, render_template, session
import mysql.connector as mysql



app = Flask(__name__)


#Keeps client-side sessions secure
app.secret_key = b'123098'

@app.route("/", methods=["POST", "GET"])
def welcome():

    if request.method == "POST":
        input_username = request.form['username']
        input_password = request.form['password']

        db = mysql.connect(
            host = "database",
            user = "login",      
            passwd = "84Cb%#s9@01",    
            database = "injectiondb")

        #use try to block errors.
        try:
            mycursor = db.cursor()
            sql = f"SELECT username, password FROM user WHERE username = '{input_username}' and password = '{input_password}';"
            mycursor.execute(sql)
            userinfo = mycursor.fetchone()
            mycursor.close()

            if userinfo:
                return render_template("index.html", text = "Nice job! \n flag{1nJ3cT3D}")

            else:
                return render_template("index.html", text = "Wrong username or password")
        
        #handel errors e.g. Server error.
        except:
            return render_template("index.html", text = "Wrong username or password" )

    return render_template("index.html", text = "Welcome to the Lethal Injection challenge" )

if __name__== "__main__":
    app.run()