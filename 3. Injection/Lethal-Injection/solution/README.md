# Lethal-Injection

## solution
----------- Path to flag: -----------

1.  Vul een simpele ' OR '1'='1 SQL injectie in de username input veld in.

2.  Vul een simpele ' OR '1'='1 injectie in het wachtwoord input veld in.

3.  Druk op de submit knop om de injectie uit te voeren. hiermee krijg je de flag te zien

flag: flag{1nJ3cT3D}


HINT: Kijk naar de beschrijving van de challenge.

HINT: Wanneer eindigd een string in een SQL query?

HINT: Hoe kan er nog een voorwaarde bij de query worden gezet?

HINT: Welke voorwaarde klopt altijd?

